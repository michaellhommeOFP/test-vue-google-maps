export function setPosition (context, position) {
  context.commit('setPosition', position)
}

export function addMarker (context, marker) {
  context.commit('addMarker', marker)
}

export function updateMarker (context, marker) {
  context.commit('updateMarker', marker)
}

export function deleteMarker (context, marker) {
  context.commit('deleteMarker', marker)
}
